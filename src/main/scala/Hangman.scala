import java.io.File
import scala.io.StdIn.readLine

package object hm {
  def wordList(fname: String): List[String] = {
    val source = io.Source.fromFile(fname)
    val words: List[String] = source.getLines.toList
    source.close()
    words
  }

  def randomWord(words: List[String]): String = {
    words(scala.util.Random.nextInt(words.length))
  }

  def alphaSet: Set[Char] = {
    ('A' to 'Z').toSet
  }

  def alphaNumberSet: Set[Char] = {
    ('0' to '9').toSet
  }

  def wordSplit(word: String): List[Char] = {
    word.toList
  }

  def wordSplitAlphaSet(word: String): List[Char] = {
    var letter: List[Char] = word.toList
    val response = letter.map(a => {
      var res = a
      if (alphaSet.contains(a)) {
        res = "_".toCharArray.head
      }
      res
    })
   response
  }

  def wordJoin(wordlist: List[Char]): String = {
    wordlist.mkString(" ")
  }


  def applyGuess(letter: Char, guesslist: List[Char], hanglist: List[Char]): List[Char] = {
    guesslist.zip(hanglist).map({ case (g, h) => if (letter == h) h else g })
  }

  def stripExtension(str: String): String = {
    if (str == null) return null;
    val pos = str.lastIndexOf(".");
    if (pos == -1) return str;
    return str.substring(0, pos);
  }

  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }

  def selectCategory(): String = {
    println("Please select category ")
    val listFile = getListOfFiles("src/resources/category");
    var exitCategory = false
    var index = 0;
    while (!exitCategory) {
      listFile.zipWithIndex.foreach { case (item, index) =>
        println(s"$index => ${stripExtension(item.getName)}")
      }
      print("Select number category: ")
      val inputCategory = readLine()
      var letter: List[Char] = inputCategory.toList
      if (!alphaNumberSet.contains(letter.head)) {
        println("Error Please input number category")
      } else if (inputCategory.toInt >= listFile.length) {
        println("Error Please input category in list")
      } else {
        exitCategory = true;
        index = inputCategory.toInt;
      }
    }
    return listFile(index).getPath
  }
}

object Hangman extends App {
  println("Welcome to the Hangman word guessing game.")
  val fname = hm.selectCategory();
  var words = hm.wordList(fname)
  val alphaset: Set[Char] = hm.alphaSet
  val fmtsummary = "%s  Wins : %2d  Losses : %2d  Score : %d"
  val fmtinput = "%s  remaining wrong guess : %2d , wrong guessed %s , Input : "
  // The game play loop.
  var fexit = false
  var wins = 0
  var losses = 0
  while (!fexit) {
    if (words.isEmpty) {
      println("Game End ")
      fexit = true;
    } else {
      println("Type 'Exit' to leave the game and Show Score, 'New' for a new game.")
      println("Good luck!\n")
      val data = hm.randomWord(words)
      words = words.filterNot(_ == data)
      val dataSplit = data.split("\\|")
      println("HINT : %s".format(dataSplit(1)))
      val hangword = dataSplit(0);
      val hanglist = hm.wordSplit(hangword.toUpperCase)
      var guesslist = hm.wordSplitAlphaSet(hangword.toUpperCase)
      var guesset: Set[Char] = Set()
      var guessInvalidSet: Set[Char] = Set()
      var guesses = 10
      var fnewgame = false
      while (!fnewgame && !fexit) {
        def checkGuess(): Unit = {
          def printSummary(message: String): Unit = {
            println("ANSWER : %s  \n".format(hm.wordJoin(hanglist)))
            println(fmtsummary.format(message, wins, losses, (wins * 100/ (wins + losses)) ))
          }

          if (hanglist == guesslist) {
            fnewgame = true
            wins += 1
            printSummary("Congratulations on your win!")
          } else {
            guesses match {
              case 1 =>
                fnewgame = true
                losses += 1
                printSummary("Too Bad! Please try again.")
              case _ => guesses -= 1
            }
          }

        }

        val line = readLine(fmtinput.format(hm.wordJoin(guesslist), guesses, guessInvalidSet)).toUpperCase

        line match {
          case "NEW" => guesses = 1; checkGuess()
          case "EXIT" => fexit = true
          case "" => Nil
          case _ => {
            val letter: List[Char] = line.toList
            if ((1 < letter.length) || !alphaset.contains(letter.head)) {
              println("Not a valid guess -> " + line)
            } else {
              if (hanglist.contains(letter.head)) {
                guesslist = hm.applyGuess(letter.head, guesslist, hanglist)
                guesses += 1
              } else {
                guessInvalidSet = guessInvalidSet + letter.head
              }
              guesset = guesset + letter.head
              checkGuess()
            }
          }
        }
      }

    }
  }

  val round = wins + losses
  println("You play a total of %2d rounds.".format(round))
  println("You score is %d".format((wins * 100 / round)))
  println((wins * 100 / round))
  println("Thank you for playing Scala Hangman!")
}
